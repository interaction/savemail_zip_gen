﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.IO.Compression;
using System.Collections.Generic;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace SaveMailZip
{
    class SaveMailZipGenerator
    {
        // StringBuilder used for error reporting. If something breaks, will be used to generate a log of what happened and where the exception occurred, but ignored otherwise
        private static StringBuilder logBuilder;

        static void Main(string[] args)
        {
            logBuilder = new StringBuilder(); 
            CreateJSON(args[0]);
        }

        /// <summary>
        /// Takes the directory that contains the PDF, JSON and condensed manifest JSON, and replaces it with a fleshed-out manifest JSON.
        /// Also ensures the files are in the correct directory sub-structure and creates the required zip file for SaveMail processing
        /// </summary>
        /// <param name="filePath">Directory containing the required files</param>
        static void CreateJSON(string filePath)
        {
            try
            {
                DirectoryInfo di = new DirectoryInfo(filePath);
                
                // Read the data from the condensed manifest into memory
                // Throw an error if the file is missing
                logBuilder.AppendLine($"FilePath = '{filePath}'");
                logBuilder.AppendLine("Reading Data from original manifest file.");
                string manifest = $@"{filePath}\savemail_manifest.json";
                if (!File.Exists(manifest))
                    throw new FileNotFoundException($"Must be a file called '{manifest}' in the directory.");
                FileInfo inputFile = new FileInfo(manifest);
                string[] lines = File.ReadAllLines(inputFile.FullName);

                // If the condensed manifest file is not in the correct format, throw an exception
                if (lines.Length != 5)
                    throw new InvalidDataException("Expected 5 rows in manifest file, received " + lines.Length.ToString() + " lines instead.");

                // Store the data in a dictionary for later access/manipulation
                logBuilder.AppendLine("Populating the manifest dictionary.");
                Dictionary<string, string> manifestParams = new Dictionary<string, string>();
                manifestParams.Add("issuer_id", lines[0]);
                manifestParams.Add("upload_ref", lines[1]);
                manifestParams.Add("archive_created", lines[2]);
                manifestParams.Add("issue_date", lines[3]);
                manifestParams.Add("item_count", lines[4]);
                // NOTE THAT THE ITEM COUNT READ FROM THE MANIFEST JSON FILE IS *NOT* THE ITEM COUNT REQUIRED IN THE NEW FILE
                // THIS VALUE IS READ BUT NOT ACTUALLY USED AS A RESULT

                // Remove the condensed manifest, as it is no longer required
                logBuilder.AppendLine("Deleting original manifest file.");
                File.Move(inputFile.FullName,di.Parent.FullName + Path.DirectorySeparatorChar + "savemail_manifest-ORIG.json");

                // Identify and grab the remaining files: the PDF and JSON
                // Throw an error if files don't exist or more than one there




                // Set up the required sub-directories for the ZIP file
                logBuilder.AppendLine("Creating sub-directory structure.");
                di.CreateSubdirectory("items");
                di.CreateSubdirectory(".savemail_manifest");

                FileInfo pdfFile, jsonFile;
                if (di.GetFiles().Length == 2)
                {
                    pdfFile = di.GetFiles("*.pdf")[0];
                    jsonFile = di.GetFiles("*.json")[0];
                }
                else
                    throw new FileNotFoundException("Did not recieve Expected 2 Files");




                // Recreate the PDF file page-by-page for Savemail purposes
                String pdfFileName = Path.GetFileNameWithoutExtension(pdfFile.FullName);
                String pdfExt = Path.GetExtension(pdfFile.FullName);
                logBuilder.AppendLine("pdfFileName = " + pdfFileName + ", pdfExt = " + pdfExt);
                PdfReader sourcePdf = new PdfReader(filePath + Path.DirectorySeparatorChar + pdfFileName + pdfExt);
                logBuilder.AppendLine("sourcePdf instantiated");
                Document document = new Document();
                PdfCopy destPdf = new PdfCopy(document, new FileStream(filePath + Path.DirectorySeparatorChar + pdfFileName + "-Fixed" + pdfExt, FileMode.CreateNew));
                logBuilder.AppendLine("destPdf created");
                destPdf.SetOpenAction("");
                document.Open();
                logBuilder.AppendLine("destPdf opened");
                for (int p = 1; p <= sourcePdf.NumberOfPages; p++)
                {
                    destPdf.AddPage(destPdf.GetImportedPage(sourcePdf, p));
                }
                logBuilder.AppendLine("destPdf copied");
                document.Close();
                sourcePdf.Close();

                // Remove the original PDF so it doesn't get zipped and rename the "Fixed" PDF to the original name
                File.Delete(di.FullName + Path.DirectorySeparatorChar + pdfFileName + pdfExt);
                logBuilder.AppendLine("Original file deleted");
                File.Move(di.FullName + Path.DirectorySeparatorChar + pdfFileName + "-Fixed" + pdfExt, di.FullName + Path.DirectorySeparatorChar + pdfFileName + pdfExt);

                // Create a StringBuilder to populate the fleshed-out manifest file and construct it from the data from the condensed one
                logBuilder.AppendLine("Adding data to the manifest StringBuilder.");
                StringBuilder manifestFile = new StringBuilder();
                manifestFile.AppendLine("{");
                manifestFile.AppendLine($"  \"issuer_id\": \"{manifestParams["issuer_id"]}\",");
                manifestFile.AppendLine($"  \"upload_ref\": \"{manifestParams["upload_ref"]}\",");
                manifestFile.AppendLine($"  \"archive_created\": \"{manifestParams["archive_created"]}\",");
                manifestFile.AppendLine($"  \"issue_date\": \"{manifestParams["issue_date"]}\",");
                manifestFile.AppendLine("  \"archive_root\": \"./items\",");
                manifestFile.AppendLine("  \"item_count\": 2,");
                manifestFile.AppendLine("  \"items\": [");
                manifestFile.AppendLine("    {");
                manifestFile.AppendLine("      \"item_type\": \"ITEM_CONTENT\",");
                manifestFile.AppendLine($"      \"item_ref\": \"{manifestParams["upload_ref"].Replace("Ref", "Content")}\",");
                manifestFile.AppendLine($"      \"item\": \"{pdfFile.Name}\",");
                manifestFile.AppendLine($"      \"hash\": \"{CalculateMD5(pdfFile.FullName)}\"");
                manifestFile.AppendLine("    },");
                manifestFile.AppendLine("    {");
                manifestFile.AppendLine("      \"item_type\": \"ITEM_INDEX\",");
                manifestFile.AppendLine("      \"index_type\": \"SAVEMAIL_DFT\",");
                manifestFile.AppendLine($"      \"item_ref\": \"{manifestParams["upload_ref"].Replace("Ref", "Index")}\",");
                manifestFile.AppendLine($"      \"item\": \"{jsonFile.Name}\",");
                manifestFile.AppendLine($"      \"hash\": \"{CalculateMD5(jsonFile.FullName)}\"");
                manifestFile.AppendLine("    }");
                manifestFile.AppendLine("  ]");
                manifestFile.AppendLine("}");

                // Create the new manifest file, in the new manifest sub-directory
                logBuilder.AppendLine("Creating new manifest file in new sub-directory.");
                File.WriteAllText(filePath + Path.DirectorySeparatorChar + ".savemail_manifest" + Path.DirectorySeparatorChar + ".savemail_manifest.json", manifestFile.ToString());

                // Move the original files into the new items sub-directory
                logBuilder.AppendLine("Moving original files into new sub-directory.");
                File.Move(pdfFile.FullName, filePath + Path.DirectorySeparatorChar + "items" + Path.DirectorySeparatorChar + pdfFile.Name);
                File.Move(jsonFile.FullName, filePath + Path.DirectorySeparatorChar + "items" + Path.DirectorySeparatorChar + jsonFile.Name);

                // Generate the name for the new zip file, including time-stamp
                DateTime now = DateTime.Now;
                string zipFile = Path.DirectorySeparatorChar + jsonFile.Name.Replace(".json", string.Empty) + now.ToString("_yyyyMMdd_HHmmss") + ".zip";

                // Create the zip in the parent directory, then move it back into the correct directory when done
                logBuilder.AppendLine("Creating the zip file in the parent directory.");
                ZipFile.CreateFromDirectory(filePath, di.Parent.FullName + zipFile);
                logBuilder.AppendLine("Moving the zip file back into the original directory.");
                File.Move(di.Parent.FullName + zipFile, di.FullName + zipFile);

                /*String pdfPath = "";
                String pdfNewPath = "";
                PdfReader reader = new PdfReader(pdfPath);
                PdfStamper stamper = new PdfStamper(reader, new FileStream(pdfNewPath, FileMode.Create));
                stamper.Writer.SetPdfVersion(PdfWriter.PDF_VERSION_1_3);
                stamper.Close();*/
            }
            catch (Exception ex)
            {
                // Put the details of the exception into the log and write it into the original directory
                logBuilder.AppendLine("EXCEPTION THROWN!");
                logBuilder.AppendLine("Details:" + ex.Message + "\n" + ex.StackTrace);
                File.WriteAllText(filePath + Path.DirectorySeparatorChar + "ErrorLog.txt", logBuilder.ToString());
            }
        }

        /// <summary>
        /// Takes the filename and generates an MD5 hash code for it
        /// </summary>
        /// <param name="filename">The file name to use to generate the hash code</param>
        /// <returns>The hash code for the given file name</returns>
        static string CalculateMD5(string filename)
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(filename))
                {
                    var hash = md5.ComputeHash(stream);
                    return BitConverter.ToString(hash).Replace("-", "").ToLowerInvariant();
                }
            }
        }
    }
}
